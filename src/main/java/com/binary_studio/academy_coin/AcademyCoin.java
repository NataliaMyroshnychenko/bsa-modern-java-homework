package com.binary_studio.academy_coin;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {

	}

	/**
	 * Calculating the maximum profit a lucky investor can get.
	 * @param prices - Stream of non-negative integers prices
	 * @return - maximum profit a lucky investor can get.
	 */
	public static int maxProfit(Stream<Integer> prices) {
		AtomicInteger profit = new AtomicInteger();

		prices.reduce(0, (previousItem, currentItem) -> {

			if (previousItem <= 0) {
				return currentItem;
			}

			if (previousItem < currentItem) {
				profit.addAndGet(-previousItem + currentItem);
			}

			return currentItem;
		});

		return profit.get();
	}

}
