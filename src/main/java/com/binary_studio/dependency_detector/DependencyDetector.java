package com.binary_studio.dependency_detector;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	/**
	 * Checking for circular dependencies between libraries.
	 * @param libraries - A Dependency List object that contains a general list of project
	 * dependencies (libraries) and a list of internal dependencies between libraries
	 * (dependencies).
	 * @return - If there are no circular dependencies and the project can be built,
	 * return true. Otherwise, return false.
	 */
	public static boolean canBuild(DependencyList libraries) {

		boolean canBuild = true;

		if (!libraries.libraries.isEmpty() && !libraries.dependencies.isEmpty()) {

			for (String library : libraries.libraries) {

				if (canBuild) {

					boolean isFirstContainInLibrary = false;
					boolean isSecondContainInLibrary = false;

					for (String[] dependency : libraries.dependencies) {

						isFirstContainInLibrary = dependency[0].equals(library) || isFirstContainInLibrary;

						if (isFirstContainInLibrary) {
							isSecondContainInLibrary = dependency[1].equals(library);
						}

						if (isFirstContainInLibrary && isSecondContainInLibrary) {
							canBuild = false;
							break;
						}
					}
				}
			}
		}
		return canBuild;
	}

}
